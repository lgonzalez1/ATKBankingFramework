import XCTest
@testable import ATKBankingFramework

final class ATKBankingFrameworkTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ATKBankingFramework().text, "Hello, World!")
    }
}
